# README #

This is a project to learn the Addy Osmani [JS Design Patterns proposal](https://addyosmani.com/resources/essentialjsdesignpatterns/book/).

### Summary ###

* Quick summary
* Version 0.0.0
* [Learn Markdown](https://bitbucket.org/tutorials/markdowndemo)

### How do I get set up? ###

* You could compile JS with Babel for ex: `npx babel --watch 'src/' --out-dir 'public/'`
* Optionally you can use yarn (instead npm) with `npm install yarn`
* Dependencies:
   - babel-cli
   - babel-core
   - babel-loader
   - babel-preset-env
   - file-loader
   - html-webpack-plugin
   - webpack
   - webpack-cli
   - webpack-dev-server
* How to run tests

### How to RUN our localhost ###

* With `yarn start` or `npm start`

### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines
