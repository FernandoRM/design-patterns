// Each of the following options will create a new empty object:
 
var newObject = {};

console.log('newObject: ', newObject);
 
// or
var newObject = Object.create( Object.prototype );
console.log('newObject with prototype: ', newObject);
 
// or
var newObject = new Object();
console.log('newObject with new Object(): ', newObject);

var otherObj = {};
