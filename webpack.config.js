const path = require('path');

module.exports = {
  mode: 'development',
  entry: [
    './src/index.js',
    './src/class.js',
    './src/new-object.js',
    './src/object-assign.js',
    './src/object-inheritance.js',
    './src/constructor.js',
    './src/constructor-prototypes.js',
    './src/module.js',
  ],
  output: {
    path: path.resolve(__dirname, 'public'),
    filename: 'bundle.js'
  },
  module: {
    rules: [
      {
        test: /\.js$/,
        exclude: /(node_modules)/,
        use: {
          loader: 'babel-loader',
          options: {
            presets: ['babel-preset-env']
          }
        }
      },
      {
        test: /\.(png|jpg|gif|html)$/,
        use: [
          {
            loader: 'file-loader',
            options: {
              name: 'index.html'
            }
          }
        ]
      }          
    ]
  },
  devServer: {
    contentBase: ['./'],
    watchContentBase: true,
    port: 3020,
    open: true
  }
};